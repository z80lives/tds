var canvas = document.getElementById('canvas');
var ctx   = canvas.getContext("2d");

var player = {
    ctx: ctx,
    x: 0,
    y: 0,
    draw: function(ctx){
	ctx.fillStyle = "green";
	//ctx.fillRect(this.x, this.y, 20, 20);
	ctx.beginPath();
	ctx.arc(this.x, this.y, 40, 0, 2* Math.PI, false);
	ctx.fill();
    }
};

var tileSys = {
    startX: 0,
    endX:   0,
    tilew: 32,
    tileh: 32,
    draw: function(ctx, map){
	var data = map.data;
	var height = data.length;
	var width = data[0].length;
	ctx.fillStyle = "red";
	var fillOffset = 2;
	for(var y = 0; y< height; y++){
	    for(var x = 0; x< width; x++)
	    {
		
		ctx.strokeRect(x*this.tilew,
			       y*this.tileh,
			       this.tilew,
			       this.tileh);
		if(data[y][x])
		ctx.fillRect(x*this.tilew+fillOffset,
			     y*this.tileh+fillOffset,
			     this.tilew  -fillOffset,
			     this.tileh  -fillOffset);
		
	    }	    
	}
    }
};

var map =  {
    tiley:3,
    tilex: 3,
    data: [
	[0, 0, 0, 0, 0],	
	[0, 1, 0, 0, 0],
	[0, 1, 1, 0, 0],
	[0, 1, 0, 0, 0],
	[0, 0, 0, 0, 0],
	[0, 0, 0, 0, 0]	
    ]
};



var p = player;

var gamepads = {};

var gpButtonMap = {'fire': 0,
		   'cancel': 1,
		   'start': 7,
		   'up' : 9
		  };


function gamepadHandler(event, connecting)
{
    var gamepad = event.gamepad;

    if(connecting){
	gamepads[gamepad.index] = gamepad;
    }
    else
    {
	delete gamepads[gamepad.index];
    }
	
}

window.addEventListener("gamepadconnected", function(e){gamepadHandler(e, true)} , false);

redraw();

window.setInterval(process, 50);
window.setInterval(redraw, 60);

function process(){
    if(p.x < 0)
	p.x = canvas.width;
    if(p.x > canvas.width)
	p.x = 0;
    if(p.y < 0)
	p.y = canvas.height;
    if(p.y > canvas.height)
	p.y = 0;

    p.x += 5 * Math.cos(Math.PI/Math.random());
    p.y += 5 * Math.sin(Math.PI/Math.random());
}

function redraw(){    
    //ctx.fillStyle = "black";
    //ctx.fillRect(0,0,640,480);
    ctx.clearRect(0,0, canvas.width, canvas.height);
    tileSys.draw(ctx, map);
    if(gamepads[0])
	console.log(gamepads[0].buttons[gpButtonMap.up]);
    //p.draw(ctx);
}

function mouseMove(e){
    if(e.clientX < canvas.clientWidth && e.clientY < canvas.clientHeight){
	//player.x = (e.clientX / canvas.clientWidth) * canvas.width;
	//player.y = (e.clientY / canvas.clientHeight) * canvas.height;
	//redraw();
	//console.log(player.x);
    }
}

document.onmousemove = mouseMove;


