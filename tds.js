var canvas = document.getElementById('canvas');
var ctx   = canvas.getContext("2d");
var inputState = {
    up: false,
    down: false,
    left: false,
    right: false,
    fire: false,
    start: false
};


var transformer = {
    rotate: function(vec, noff){
	var tmpx = vec.x * Math.cos(noff) - vec.y * Math.sin(noff);
	var tmpy = vec.x * Math.sin(noff) + vec.y * Math.cos(noff);
	return {x: tmpx, y: tmpy};
    },
    translate: function(vec1, vec2){
	return {
	    x: (vec1.x+vec2.x),
	    y: (vec1.x+vec2.y)
	};
    }
};

var blockTiles = [];
var plyrImg = new Image();
var rockImg  = new Image();
var bulletImg = new Image();




plyrImg.src = "sprites/person.png";
rockImg.src = "sprites/rock.png";
bulletImg.src = "sprites/bullet.png";

//constructor for sprite
function sprite(options)
{
    var that = {};
    that.context = options.context;
    that.width   = options.width;
    that.height  = options.height
    that.img     = options.img;
    that.sx      = options.sx;
    that.sy      = options.sy;
    that.frame   = options.frame;
    that.states  = options.states;
    
    that.startFrame = 0;
    
    that.currentFrame = 0
    that.finalFrame = options.lastFrame;    
    that.lastFrame = options.lastFrame;
    that.currentState = options.currentState;
    that.animState = 'loop';

    that.animate = function(){
	if(this.frame > this.lastFrame)
	    this.frame = this.startFrame;	
	this.sx = this.frame * this.width;
    };

    that.setState = function(vstate){
	this.currentState = vstate;
	var state = this.states[vstate];
	this.startFrame = state.start;
	this.lastFrame  = state.end;
	
	this.frame = this.startFrame;	
    }

    that.draw = function(spr){
	ctx.save();
	var offx = 0;
	var offy = 0;
	
	if(typeof(spr.offx) != 'undefined')
	    offx = spr.offx;
	if(typeof(spr.offy) != 'undefined')
	    offy = spr.offy;
	ctx.translate(spr.x,spr.y);
	ctx.rotate(spr.rot);	
	ctx.drawImage(this.img,
		      this.sx,
		      0,
		      this.width,
		      this.height,
		      (-1* (this.width/2))-offy , //, (this.spr.width),
		      -1* (this.height/2) , //(this.spr.height),
		      this.width,
		      this.height		      
		     );

	ctx.restore();
    }

    return that;
}


var bullet = function (){
    return {
	x: 13,
	y: 13,
	offx: 20,
	offy: -40,
	org: {x: 0, y:0},
	speed: 18,
	rot: 0,
	face: 0,
	state: 'idle',
	spr: sprite({
	    sx:0,
	    sy: 0,
	    frame: 0,
	    context: ctx,
	    width: 13,
	    height: 13,
	    img: bulletImg,
	    lastFrame: 1,
	    states: {'idle': {start: 0, end:0, loop: false} }
	}),
	
    draw: function(ctx){	
	
	if(this.spr.currentState != this.state)
	    this.spr.setState(this.state);
 	this.spr.animate();
	this.spr.draw({x: this.x, y: this.y, rot: this.rot, offx: this.offx, offy:this.offy});
	
    },
    move: function(){
	var vec = {x: this.x, y: this.y};
	var delta =  {x: 0, y:-1};
	delta = transformer.rotate(delta, this.rot);
	
	var res = {x:this.x, y:this.y};
	res.x += this.speed * delta.x;
	res.y += this.speed * delta.y;
	
	this.x = res.x;
	this.y = res.y;
	this.org.x = res.x + this.spr.width/ 2;
	this.org.y = res.y + this.spr.height/2;
    }
	
    };
}

var player = {
    ctx: ctx,
    x: 100,
    y: 100,
    rot: 0,
    face: 0,
    speed: 0,
    state: 'idle',
    armState: 'ready',
    recoilTime: 500,
    spr: sprite(
	{
	    sx: 0,
	    sy: 0,
	    frame: 0,
	    context: ctx,
	    width: 100,
	    height: 100,
	    img: plyrImg,
	    lastFrame:  2,
	    states: {
		'idle': {start: 0, end: 0, loop: false},
		'walk': {start: 0, end: 2, loop: true},
		'strafe': {start: 3, end: 5, loop: true},
		'fire' : {start: 4, end: 5, loop: false}
	    }
	}
    ),    
    org: {x: 0,
	  y: 0},
    draw: function(ctx){	
	
	if(this.spr.currentState != this.state)
	    this.spr.setState(this.state);
 	this.spr.animate();
	this.spr.draw({x: this.x, y: this.y, rot: this.rot});	
    },
    animate: function(){
	this.spr.frame += 1;
    },    
    move: function(){
	var vec = {x: this.x, y: this.y};
	var delta =  {x: 0, y:-1};

	if(this.state == "strafe")
	    this.face = this.strafe_dir *0.5 * Math.PI;
	else
	    this.face = 0;
	//delta.x = Math.sin(this.rot)*3.4;
	//delta.y = Math.cos(this.rot)*3.4;
	delta = transformer.rotate(delta, this.rot+this.face);
	var res = {x:this.x, y:this.y};
	res.x += this.speed * delta.x;
	res.y += this.speed * delta.y;
	
	//console.log(delta, vec);
	//var res = transformer.translate(vec, delta);
	this.x = res.x;
	this.y = res.y;
	this.org.x = res.x + this.spr.width/ 2;
	this.org.y = res.y + this.spr.height/2;
    },
    savePos: function(){
	this.oldx  = this.x;
	this.oldy  = this.y;
	this.oldorgx = this.org.x;
	this.oldorgy = this.org.y;
    },
    loadPos: function(){
	this.x = this.oldx;
	this.y = this.oldy;	
	this.org.x = this.oldorgx;
	this.org.y = this.oldorgy;
    }
};

var box = {
    spr: sprite(
	{
	    sx: 0,
	    sy: 0,
	    frame: 0,
	    context: ctx,
	    width: 100,
	    height: 100,
	    img: rockImg,
	    lastFrame:  5,
	    states: {
		'idle': {start: 0, end: 0, loop: false},
	    }
	}
    ),

    draw: function(ctx){	
	
	if(this.spr.currentState != this.state)
	    this.spr.setState(this.state);
 	this.spr.animate();
	this.spr.draw({x: this.x, y: this.y, rot: this.rot});
	
    }
};

var tileSys = {
    startX: 0,
    endX:   0,
    tilew: 100,
    tileh: 100,
    draw: function(ctx, map){
	var data = map.data;
	var height = data.length;
	var width = data[0].length;
	ctx.fillStyle = "red";
	var fillOffset = 2;
	for(var y = 0; y< height; y++){
	    for(var x = 0; x< width; x++)
	    {

		//draw border
		ctx.strokeRect(x*this.tilew,
			       y*this.tileh,
			       this.tilew,
			       this.tileh);
		if(data[y][x] == 1)
		ctx.fillRect(x*this.tilew+fillOffset,
			     y*this.tileh+fillOffset,
			     this.tilew  -fillOffset,
			     this.tileh  -fillOffset);
		if(data[y][x] == 2)
		    ctx.drawImage(box.spr.img,
				  x*this.tilew,
				  y*this.tileh);
		
	    }	    
	}
    }
};

var map =  {
    tiley:3,
    tilex: 3,
    init: function(){
	blockTiles = [2];
    },
    data: [
	[0, 0, 0, 0, 2],	
	[0, 0, 0, 2, 2],
	[0, 0, 0, 0, 2],
	[0, 0, 0, 0, 2],
	[0, 0, 0, 2, 2],
	[2, 2, 2, 2, 2]	
    ]
};


var gameManager = {
    lvl: 0,
    map: '',
    projectiles: [],
    bound:
    { width: 640, height: 480 },
    hitTest: function(spr1, spr2)
    {
	return false;
    },
    getTilePos: function (spr){
	var ret = {x:Math.round(spr.org.x / tileSys.tilew) -1,
		   y:Math.round(spr.org.y / tileSys.tileh) -1};	
	return ret;
    },
    hitMap: function(spr, map){
	var sprMapPos = this.getTilePos(spr);

	/*console.log(
	    sprMapPos,

	);*/

	for(var i=0; i< blockTiles.length; i++){
	    if( map.data[sprMapPos.y] ){
		data = map.data[sprMapPos.y][sprMapPos.x];
		if(data == blockTiles[i])
		    return true;
	    }
	}
	return false;
    }    
};



var p = player;
p.x = 130;
p.y = 130;
map.init();

redraw();
window.setInterval(process, 50);
window.setInterval(redraw, 60);
window.setInterval(animate, 300);

function animate(){
    p.animate();
}

function process(){
    p.savePos();

    //readInput
    if(inputState.up)
	p.speed = 5;
    else if(inputState.down)
	p.speed = -5;
    else
	p.speed = 0;

    if(inputState.fire && p.armState == 'ready'){
	p.armState = "fire";
    }

    if(inputState.left || inputState.right){
	if(inputState.left)
	    p.strafe_dir = -1;
	else
	    p.strafe_dir = 1;
	p.speed = 3;
	p.state = "strafe";

    }

    //enforce limits
    if(p.x < 0)
	p.x = canvas.width;
    if(p.x > canvas.width)
	p.x = 0;
    if(p.y < 0)
	p.y = canvas.height;
    if(p.y > canvas.height)
	p.y = 0;

    p.move();
    
    if(gameManager.hitMap(p, map)){
	p.loadPos();
	p.speed = 0;
	p.state = "idle";
    }

    if(p.state == 'strafe' && p.speed > 0 )
    {
	if(p.speed < 0.1)
	    p.rot = 2;
	p.speed -= 0.01;
	
    }else if(p.speed != 0){
	p.speed -= 0.01;
	p.state = 'walk';
    }else if(p.armState=='ready'){
	p.state = 'idle';
	p.face = 0;
    }

    if(p.armState == 'fire'){
	p.state = 'fire';
	p.armState = 'recoil';
	var bull = new bullet();
	bull.x = p.x;
	bull.y = p.y;
	bull.offx = 13/2;
	bull.offy = 13/2;
	
	bull.rot = p.rot;
	gameManager.projectiles.push(bull);
	window.setTimeout(function(){p.armState = 'ready'}, p.recoilTime);
    }

}

function redraw(){    
    //ctx.fillStyle = "black";
    //ctx.fillRect(0,0,640,480);
    ctx.clearRect(0,0, canvas.width, canvas.height);
//    tileSys.draw(ctx, map);
//    gameManager.projectiles
    
    gameManager.projectiles.forEach(function(e, index, object){
	e.move();
	if(e.x < 0 || e.y < 0)
	    object.splice(index,1);
	
	if(e.x > gameManager.bound.width || e.y > gameManager.bound.height)
	    object.splice(index,1);
	
	e.draw(ctx);

    });

        p.draw(ctx);
}


