console.log(canvas);

var tCursor = {
    x: canvas.width/2,
    y: canvas.height/2,
    active: false,
    attached: null,
    attachPlayer: function(plyr){
	this.attached = plyr;
    }
};

var mouseMove =  function(e){

    if(e.clientX < canvas.clientWidth && e.clientY < canvas.clientHeight){
	//var x = (e.clientX / canvas.clientWidth) * canvas.width;
	//var y = (e.clientY / canvas.clientHeight) * canvas.height;
	var x = e.clientX;
	var y = e.clientY;
	//
	//console.log(e.clientX,y);
	if(tCursor.attached){
	    var deltax = tCursor.attached.x-x;
	    var deltay = tCursor.attached.y-y;	    


	    tCursor.attached.rot = Math.atan2(deltay, deltax) - (Math.PI/2);
	}
    }

}

canvas.onmousemove = mouseMove;
tCursor.attached = p;

//Keyboard event handler
window.addEventListener("keydown",
			function(e){
			    const keyName = e.key;
			    if(e.key == 'w')
				inputState.up = true;
			    if(e.key == "s")
				inputState.down = true;
			    if(e.key == " ")
				inputState.fire = true;
			    if(e.key == "a")
				inputState.left = true;
			    if(e.key == "d")
				inputState.right = true;
			    
			    //	console.log('Down');
			});

window.addEventListener("keyup",
			function(e){
			    inputState.up = false;
			    inputState.down = false;
			    inputState.left = false;
			    inputState.right = false;
			    inputState.fire = false;
			});

