var gamepads = {};

var gpButtonMap = {'fire': 0,
		   'cancel': 1,
		   'start': 7,
		   'up' : 9
		  };


function gamepadHandler(event, connecting)
{
    var gamepad = event.gamepad;

    if(connecting){
	gamepads[gamepad.index] = gamepad;
    }
    else
    {
	delete gamepads[gamepad.index];
    }
	
}

window.addEventListener("gamepadconnected", function(e){gamepadHandler(e, true)} , false);

function readInput(){
    if(gamepads[0])
	console.log(gamepads[0].buttons[gpButtonMap.up]);

}
